import {
    Route,
    BrowserRouter as Router,
    Switch,
    Redirect,
} from "react-router-dom";
import Login from "./components/Login/Login";
import Home from "./components/Home/Home";
import PostDetailContainer from "./components/PostDetailContainer/PostDetailContainer";
import CreatePost from "./components/CreatePost/CreatePost";
import reducer from "./reducer/index";
import { createStore } from "redux";
import { Provider } from "react-redux";

function App() {
    const store = createStore(reducer);
    return (
        <Provider store={store}>
            <Router>
                <Switch>
                    <Redirect exact from="/" to="/home" />
                    <Route path="/login" component={Login} />
                    <Route path="/home" component={Home} />
                    <Route
                        path="/detail/:postId"
                        component={PostDetailContainer}
                    />
                    <Route
                        path="/createupdate/:postId?"
                        component={CreatePost}
                    />
                </Switch>
            </Router>
        </Provider>
    );
}

export default App;
