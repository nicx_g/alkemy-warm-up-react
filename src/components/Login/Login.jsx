import { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import useUser from '../../hooks/useUser';
import SignupForm from './SignupForm/SignupForm';

const Login = () => {
    const {isLogged} = useUser();
    const history = useHistory()
    useEffect(() => {
        if(isLogged) history.push("/home")
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <div className="min-vh-100 container d-flex justify-content-center align-items-center">
            <div className="bg-light p-2 p-sm-4 w-auto rounded d-flex flex-column justify-content-center align-items-center">
                <img src="https://campus.alkemy.org/logos/d_logo_small.png" alt="logo alkemy" />
                <h1 className='fs-5 text-center mb-2 mb-sm-2'>¡Armemos un blog!</h1>
                <h2 className='fs-6 text-center mb-2 mb-sm-2'>Inicia sesión</h2>
                <SignupForm />
            </div>
        </div>
    )
}
export default Login;