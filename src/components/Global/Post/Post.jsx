import { Link, useHistory } from 'react-router-dom';
import usePosts from '../../../hooks/usePosts';

const Post = ({ post, isList }) => {
    const { deletePost, setAdditionalInfo } = usePosts();
    const history = useHistory();
    const handleClick = (id) => {
        if (isList) {
            deletePost(id)
                .then(resp => setAdditionalInfo({ deleted: true }))
                .catch(error => setAdditionalInfo({ deleted: false }))
        } else {
            deletePost(id)
                .then(resp => {
                    setAdditionalInfo({ deleted: true })
                    history.push("/home")
                })
                .catch(error => setAdditionalInfo({ deleted: false }))
        }
    }

    return (
        <div className="card m-2">
            <div className="card-body">
                <h5 className={`card-title ${isList ? 'text-center' : 'fs-3'}`}>{post.title}</h5>
                {isList ?
                    <Link
                        to={`/detail/${post.id}`}
                        className="w-100 m-1 btn btn-outline-primary"
                    >Ver detalle</Link> :
                    <p className="card-text">{post.body}</p>
                }
                <div className="d-flex">
                    <button
                        className={`m-1 ${isList ? 'w-100' : ''} btn btn-danger`}
                        onClick={() => handleClick(post.id)}
                    >Eliminar</button>
                    <Link
                        to={`/createupdate/${post.id}`}
                        className={`m-1 ${isList ? 'w-100' : ''} btn btn-secondary`}
                    >Editar</Link>
                </div>
            </div>
        </div>
    )
}

export default Post;