import { Link } from 'react-router-dom';

const AsideMenu = () => {
    return (
        <div className="my-4">
            <ul className="nav d-flex flex-column">
                <li className="fs-6 text-center text-muted fw-bold">Navegación</li>
                <li className="text-center">
                    <Link
                        to="/home"
                        className="my-2 btn btn-primary"
                    >Ir al inicio</Link>
                </li>
                <li className="fs-6 text-center text-muted fw-bold">Acciones</li>
                <li className="text-center">
                    <Link
                        to="/createupdate"
                        className="my-2 btn btn-primary"
                    >Crear una nota</Link>
                </li>
            </ul>
        </div>
    )
}

export default AsideMenu;