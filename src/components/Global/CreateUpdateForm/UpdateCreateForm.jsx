import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import usePosts from '../../../hooks/usePosts';

const validate = (values) => {
    let errors = {};
    if (!values.title) errors.title = "Requerido"
    if (!values.body) errors.body = "Requerido"
    return errors;
}

const CreateUpdateForm = ({ postId }) => {
    const history = useHistory()
    const { createUpdatePost, getPostById } = usePosts()
    const [utils, setUtils] = useState({
        error: false,
        success: false,
        type: ""
    })
    const [post, setPost] = useState({});
    useEffect(() => {
        if (postId) {
            getPostById(postId)
                .then(resp => setPost(resp))
                .catch(err => setUtils({
                    ...utils,
                    error: "No existe ese post, intenta creándolo",
                }))
        } else setPost('')
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [postId])

    return (
        <Formik
            enableReinitialize={true}
            initialValues={{
                id: post?.id ?? '',
                userId: post?.userId ?? '',
                title: post?.title ?? '',
                body: post?.body ?? '',
            }}
            validate={validate}
            onSubmit={values => {
                if (typeof utils.error === "string") return;
                if (postId) {
                    createUpdatePost(values, true)
                        .then(resp => {
                            setUtils({
                                error: false,
                                success: true,
                                type: 'edit'
                            })
                            setTimeout(() => {
                                history.push(`/detail/${values.id}`)
                            }, 3000)
                        })
                        .catch(err => setUtils({
                            ...utils,
                            error: true,
                            success: false
                        }))
                } else {
                    createUpdatePost(values, false)
                        .then(resp => {
                            setUtils({
                                error: false,
                                success: true,
                                type: 'create'
                            })
                            setTimeout(() => {
                                history.push(`/detail/${resp.data.id}`)
                            }, 3000)
                        })
                        .catch(err => setUtils({
                            ...utils,
                            error: true,
                            success: false
                        }))
                }
            }}
        >
            <Form>
                <div className="my-2">
                    <label htmlFor="title">Título</label>
                    <Field
                        id="title"
                        name="title"
                        type="text"
                        placeholder="Inserta el título de tu post"
                        className="form-control"
                    />
                    <ErrorMessage
                        className="form-text text-danger"
                        component="div"
                        name="title"
                    />
                </div>
                <div className="my-2">
                    <label htmlFor="body">Cuerpo</label>
                    <Field
                        id="body"
                        name="body"
                        as="textarea"
                        type="text"
                        placeholder="Inserta el cuerpo de tu post"
                        className="form-control"
                    />
                    <ErrorMessage
                        className="form-text text-danger"
                        component="div"
                        name="body"
                    />
                </div>
                <button
                    type="submit"
                    disabled={typeof utils.error === "string"}
                    className="btn btn-primary"
                >{postId ? "Editar post" : "Crear post"}</button>
                {utils.success ?
                    <div className="m-2 d-flex justify-content-center align-items-center">
                        <div className="alert alert-success" role="alert">
                            {utils.type === "edit" ? "Tu post fue editado con éxito, te redigiremos al post en cuestión" : "Post creado exitosamente, te redigiremos al post en cuestión"}
                        </div>
                    </div> :
                    utils.error ?
                        <div className="m-2 d-flex justify-content-center align-items-center">
                            <div className="alert alert-danger" role="alert">
                                {utils.type === "edit" ?
                                    "Ocurrió un error al editar tu post" :
                                    typeof utils.error === "string" ?
                                        utils.error :
                                        "Algo salió mal en la creación del post"}
                            </div>
                        </div> :
                        null
                }
            </Form>
        </Formik >
    )
}
export default CreateUpdateForm;