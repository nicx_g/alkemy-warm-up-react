const ToastAlert = ({ msg }) => {
    return (
        <div className="position-fixed bottom-0 end-0 p-4" style={{ zIndex: "1" }}>
            <div className="alert alert-success alert-dismissible fade show" role="alert">
                {msg}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        </div>
    )
}

export default ToastAlert;