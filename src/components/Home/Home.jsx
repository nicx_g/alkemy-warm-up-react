import { useEffect } from 'react'
import { useHistory } from 'react-router-dom';
import useUser from '../../hooks/useUser';
import AsideMenu from '../Global/AsideMenu/AsideMenu';
import PostsContainer from './PostsContainer/PostsContainer';

const Home = () => {
    const { isLogged } = useUser();
    const history = useHistory();
    useEffect(() => {
        if (!isLogged) history.push("/login")
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    return (
        <div className="min-vh-100 m-0 row">
            <div className="col-12 col-sm-4 col-lg-3 col-xl-2 bg-dark">
                <AsideMenu />
            </div>
            <div className="col-12 col-sm-8 col-lg-9 col-xl-10 bg-light">
                <PostsContainer />
            </div>
        </div>
    )
}
export default Home;