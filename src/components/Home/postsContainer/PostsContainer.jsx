import { useEffect, useState } from 'react';
import Post from '../../Global/Post/Post';
import usePosts from '../../../hooks/usePosts';
import AlertPost from '../../Global/AlertPost/AlertPost';

const Posts = () => {
    const [posts, setPosts] = useState([]);
    const [utils, setUtils] = useState({
        error: false,
    })
    const { getAllPosts, additionalInfo } = usePosts();
    useEffect(() => {
        getAllPosts()
            .then(resp => setPosts(resp))
            .catch(err => setUtils({ error: true }))
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <div className="container py-4">
            {additionalInfo.deleted ?
                <AlertPost msg="Tu post fue eliminado con éxito" /> :
                additionalInfo.deleted !== null ?
                    <AlertPost msg="Ocurrió un error al eliminar tu post, prueba otra vez" /> :
                    null
            }
            <h1 className="text fs-2">Mis posts</h1>
            <div className="row">
                {posts.length ?
                    posts.map(post => (
                        <div key={post.id} className="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3">
                            <Post post={post} isList={true} />
                        </div>
                    )) :
                    utils.error ?
                        <div className="d-flex justify-content-center align-items-center">
                            <div class="alert alert-danger" role="alert">
                                No encontré ninguna nota :( Inténtalo de nuevo más tarde
                            </div>
                        </div> :
                        <div className="d-flex justify-content-center align-items-center">
                            <div className="spinner-border text-primary" role="status">
                                <span className="visually-hidden">Loading...</span>
                            </div>
                        </div>
                }
            </div>
        </div>
    )
}
export default Posts;