import { useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router-dom';
import useUser from '../../hooks/useUser';
import usePosts from '../../hooks/usePosts';
import AsideMenu from '../Global/AsideMenu/AsideMenu';
import Post from '../Global/Post/Post';
import AlertPost from '../Global/AlertPost/AlertPost';

const PostDetailContainer = () => {
    const { isLogged } = useUser();
    const { getPostById, additionalInfo } = usePosts();
    const history = useHistory();
    const { postId } = useParams();
    const [utils, setUtils] = useState({ error: false });
    const [post, setPost] = useState({})
    useEffect(() => {
        if (!isLogged) history.push("/login")
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    useEffect(() => {
        getPostById(postId)
            .then(resp => setPost(resp))
            .catch(err => setUtils({ error: true }))
            // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [postId])
    return (
        <div className="min-vh-100 m-0 row">
            <div className="col-12 col-sm-4 col-lg-3 col-xl-2 bg-dark">
                <AsideMenu />
            </div>
            <div className="col-12 col-sm-8 col-lg-9 col-xl-10 bg-light">
                {additionalInfo.deleted ?
                    <AlertPost msg="Tu post fue eliminado con éxito" /> :
                    additionalInfo.deleted !== null ?
                        <AlertPost msg="Ocurrió un error al eliminar tu post, prueba otra vez" /> :
                        null
                }
                <div className="my-2 container">
                    <h1>Detalle de mi post</h1>
                </div>
                {Object.keys(post).length !== 0 ?
                    <Post post={post} isList={false} /> :
                    utils.error ?
                        <div className="d-flex justify-content-center align-items-center">
                            <div className="alert alert-danger" role="alert">
                                No encontré ese post, créalo o intenta con otro
                            </div>
                        </div> :
                        <div className="d-flex justify-content-center align-items-center">
                            <div className="spinner-border text-primary" role="status">
                                <span className="visually-hidden">Loading...</span>
                            </div>
                        </div>
                }
            </div>
        </div>
    )
}
export default PostDetailContainer;