import { useState } from "react";
import {useSelector, useDispatch} from 'react-redux';
import { useHistory } from "react-router-dom";
import axios from "axios";

const useUser = () => {
    const {loginToken} = useSelector(state => state.userReducer);
    const dispatch = useDispatch();
    const history = useHistory();
    const [utils, setUtils] = useState({
        loading: false,
        error: false,
    });
    const loginUser = (email, password) => {
        setUtils({
            error: false,
            loading: true,
        });
        axios
            .post(
                "http://challenge-react.alkemy.org",
                {
                    email,
                    password,
                },
                {
                    mode: "no-cors",
                    Headers: {
                        "Access-Control-Allow-Origin": "*",
                    },
                }
            )
            .then((resp) => {
                setUtils({
                    loading: false,
                    error: false,
                });
                dispatch({
                    type: "SET_TOKEN",
                    payload: resp.data.token
                })
                history.push("/home");
            })
            .catch((err) => {
                setUtils({
                    error: true,
                    loading: false,
                });
                setTimeout(() => {
                    setUtils({
                        error: false,
                        loading: false,
                    });
                }, 4000);
            });
    };

    return {
        isLogged: Boolean(loginToken),
        loginLoading: utils.loading,
        loginError: utils.error,
        loginUser,
    };
};

export default useUser;