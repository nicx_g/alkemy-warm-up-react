import {useSelector, useDispatch} from 'react-redux';
import axios from "axios";

const usePosts = () => {
    const {additionalInfo} = useSelector(state => state.postsReducer)
    const dispatch = useDispatch();
    const setAdditionalInfo = (value) => {
        dispatch({
            type: "SET_ADDITIONAL_INFO",
            payload: {deleted: value},
        })
    }
    
    const postInstance = axios.create({
        baseURL: "https://jsonplaceholder.typicode.com/posts",
    });
    const getAllPosts = async () => {
        try {
            const resp = await postInstance.get();
            return resp.data;
        } catch (error) {
            throw new Error(error);
        }
    };
    const getPostById = async (id) => {
        try {
            const resp = await postInstance.get(`${id}`);
            return resp.data;
        } catch (error) {
            throw new Error(error);
        }
    };
    const createUpdatePost = async (newPost, isEditing) => {
        try {
            if (isEditing) {
                const resp = await postInstance.put(`${newPost.id}`, {
                    id: newPost.id,
                    userId: newPost.userId,
                    title: newPost.title,
                    body: newPost.body,
                });
                return resp;
            } else {
                const resp = await postInstance.post("", {
                    userId: newPost.userId,
                    title: newPost.title,
                    body: newPost.body,
                });
                return resp;
            }
        } catch (err) {
            throw new Error(err);
        }
    };

    const deletePost = async (id) => {
        try {
            const resp = await postInstance.delete(`${id}`);
            setTimeout(() => {
                setAdditionalInfo(null)
            }, 3000);
            return resp;
        } catch (error) {
            setTimeout(() => {
                setAdditionalInfo(null)
            }, 3000);
            throw new Error(error);
        }
    };

    return {
        additionalInfo,
        setAdditionalInfo,
        getAllPosts,
        getPostById,
        createUpdatePost,
        deletePost,
    };
};
export default usePosts;
