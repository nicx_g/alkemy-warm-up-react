const initialState = {
    additionalInfo: {
        deleted: null
    }
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case "SET_ADDITIONAL_INFO":
            return {additionalInfo: action.payload}
    
        default: return state
    }
}
export default reducer;