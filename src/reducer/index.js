import {combineReducers} from 'redux'
import postsReducer from './posts';
import userReducer from './user';

const reducer = combineReducers({
    postsReducer,
    userReducer
})

export default reducer;