const initialState = {
    loginToken: localStorage.getItem("loginToken"),
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case "SET_TOKEN":
            localStorage.setItem("loginToken", action.payload);
            return {
                loginToken: localStorage.getItem("loginToken"),
            };
        default: return state
    }
};

export default reducer;